<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Blog;
use App\Models\Author;
use App\Models\Category;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       Author::truncate();
       Blog::truncate();
       Category::truncate();

       $Author=Author::factory(0)->create();
       $frontend=Category::factory()->create(['name'=>'frontend']);
       $backend=Category::factory()->create(['name'=>'backend']);

       Blog::factory(5)->create(['category_id'=>$frontend->id,]);
       Blog::factory(5)->create(['category_id'=>$backend->id,]);


    }
}
