<?php

use App\Models\Blog;
use Illuminate\Support\Facades\Route;
use App\Models\Category;
use App\Models\Author;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('blogs',[
        'blogs'=>Blog::latest()->with('author','category')->get()         //all request out with blog
    ]);
});

Route::get('/blog/{blog:title}',function(Blog $blog){
// dd($blog);
    return view('blog',[
'blog'=>$blog
    ]);
});

Route::get('/category/{category:name}',function(Category $category){
    
return view('blogs',[
'blogs'=>$category->blogs->load('author','category')               //N+1 problem

]);
});

Route::get('/author/{author}',function (Author $author){

return view('blogs',[
   'blogs'=>$author->blogs->load('category','author')                 //N+1 problem
   
]);
});

